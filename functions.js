window.onload = configureSelect;

function configureSelect() {
  document.getElementById("selectFaq").selectedIndex = 0;
  document.getElementById("selectFaq").onchange = changePage;
}

function changePage() {
  var selectElement = document.getElementById("selectFaq");
  var newPage = selectElement.options[selectElement.selectedIndex].value;
  if (newPage != "")
    window.location = newPage;
}